﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tabuada
{
    class Program
    {
        static void Main(string[] args)
        {
            int numeroDigitado;
            Console.Write("Digite um número: ");
            numeroDigitado = int.Parse(Console.ReadLine());

            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine(numeroDigitado + " x " + i + " = " + (numeroDigitado * i));
            }

            Console.ReadKey();

        }
    }
}
